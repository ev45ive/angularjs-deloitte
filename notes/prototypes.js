

function Person(name){
    this.name = name
}
Person.prototype.sayHello = function(){
    return 'Hi, my name is '+ this.name;
}

function Employee(name, salary){
    this.name = name
    this.salary = salary
}
Employee.prototype = Object.create(Person.prototype);

Employee.prototype.work = function(){
    return 'I want my '+ this.salary;
}

alice = new Employee('Alice',2500)
bob = new Employee('bob',2000)
alice.sayHello()