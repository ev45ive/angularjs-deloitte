function Scope(parent){
    this.$id = Scope.$id++
    this.$parent = parent
}
Scope.$id = 0;
Scope.prototype.$new = function(){
    var childScope = Object.create(this);
    Scope.apply(childScope,[this])
    return childScope
};

$rootScope = new Scope();
child1 = $rootScope.$new()
child2 = child1.$new()
child2B = child1.$new()
child3 = child2.$new()
