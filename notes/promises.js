function request(msg){
    return new Promise(function(resolve){

        setTimeout(function(){
            resolve(msg)
        },2000)
    }) 

}

var resPromise = request('Ala');

resPromise.then(function handleResult(resp){
    console.log( resp + ' lubi placki' );
})

resPromise.then(function handleResult(resp){
    console.log( resp + ' ma kota ' );
})
// after 2 sec
// Ala lubi placki
// Ala ma kota 

resPromise.then(console.log)
// immidiate:
// Ala


// ======

function request(msg){
  return new Promise(function(resolve){

      setTimeout(function(){
          resolve(msg)
      },2000)
  }) 

}

var resPromise = request('Ala');

var p2 = resPromise.then(function(resp){
  return resp + ' lubi placki';
})

p2.then(function(resp){
  console.log( resp + ' i ma kota ' );
})
// VM4126:18 Ala lubi placki i ma kota 

/// =====

function request(msg){
  return new Promise(function(resolve){

      setTimeout(function(){
          resolve(msg)
      },2000)
  }) 

}

var resPromise = request('Ala');

var p2 = resPromise.then(function(resp){
  return request(resp + ' lubi placki')
})

p2.then(function(resp){
  console.log( resp + ' i ma kota ' );
})
// After 4sec ( 2 requests = 2sec + 2 sec)

// VM4320:18 Ala lubi placki i ma kota 


/// =====

function request(msg, err){
  return new Promise(function(resolve, reject){

      setTimeout(function(){
          err? reject(err) : resolve(msg)
      },2000)
  }) 

}

var resPromise = request('Ala','upsss..');

var p2 = resPromise.then(function(resp){
  return request(resp + ' lubi placki')
}, function(err){ 
  return 'Anonymous ' + ' lubi placki'
})

p2.then(function(resp){
  console.log( resp + ' i ma kota ' );
})



/// =====

function request(msg, err){
  return new Promise(function(resolve, reject){

      setTimeout(function(){
          err? reject(err) : resolve(msg)
      },2000)
  }) 

}

var resPromise = request('Ala');

resPromise.then(function(resp){
  return request(resp + ' lubi placki','boom!')
})

.then(function(resp){
  console.log( resp + ' i ma kota ' );
})

.catch(function(err){
  console.log('Unexpected error '+err)
})

// Promise {<pending>}
// VM5119:22 Unexpected error boom!