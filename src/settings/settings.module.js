var settings = angular.module("settings", []);

settings.controller("settingsCtrl", function ($scope) {
  $scope.setting = {
    color: {
      name: "Color",
      value: "red",
    },
    size: {
      name: "Size",
      value: "12",
    },
    placki: {
      name: "placki",
      value: "placki",
    },
  };
  $scope.message = "";
  $scope.updateSetting = function (name, value) {
    $scope.setting[name].value = value;
    $scope.message = "Setting Saved!";
  };
});

/* camelCase settingsBox to kebab-case <settings-box> */
settings.directive("settingsBox", function ($interval, $timeout, TodosService) {
  return {
    restrict: "E", // "EACM"
    scope: {
      label: "@labelName",
      value: "<",
      onSave: "&",
    },
    bindToController:true,
    template: `<div class="card">
      <div ng-if="$ctrl.isSaving"> Please wait ... Saving changes.</div>
      <div class="card-body" ng-if="$ctrl.isEditing == false">
        {{$ctrl.label}} : {{$ctrl.value}}
        <button class="float-right" ng-click="$ctrl.isEditing = true">Edit</button>
      </div>
      <div class="card-body" ng-if="$ctrl.isEditing == true">
        {{$ctrl.label}} : <input ng-model="$ctrl.value"> </input>
        <button class="float-right" ng-click="$ctrl.save()">Save</button>
      </div>
    </div>`,
    controller: function () {
      this.isSaving = false;
      this.isEditing = false;

      // Notify parent about changes
      this.save = function () {
        this.isSaving = true;
        this.isEditing = false;

        this.onSave({ $event: this.value });

        $timeout(function () {
          this.isSaving = false;
        }.bind(this), 2000);
      };
    },
    controllerAs: "$ctrl",
    link: function (scope, $elem, $attr, $ctrl) {
      $elem.on("keyup", function (event) {
        scope.$apply(function () {
          if (event.key == "Escape") {
            $scope.vm.isEditing = false;
          }
        });
      });
    },
  };
});
