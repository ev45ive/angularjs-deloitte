var todos = angular.module("todos", []);


todos.constant("todosData", [])

todos.config(function (todosData) {
  console.log('todos config')
});

todos.run(function (settings, TodosService) {
  console.log('todos run')

});

todos.service("TodosService", function TodosService($filter, todosData) {
  this.todos = {
    list: todosData,
    archived: [],
  };

  this.addTodo = function (todo) {
    this.todos.list.push(angular.copy(todo));
  };

  this.clearCompleted = function () {
    var filterService = $filter("filter");

    var notCompleted = filterService(this.todos.list, {
      completed: false,
    });

    var completed = this.todos.list.filter(function (todo) {
      return todo.completed;
    });

    this.todos.list = notCompleted;
    this.todos.archived = this.todos.archived.concat(completed);
  };
});
