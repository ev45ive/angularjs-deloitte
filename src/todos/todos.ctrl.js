angular
  .module("todos")
  .controller("todosCtrl", function ($scope, $filter, TodosService) {
    $scope.mode = "todos";

    $scope.todos = TodosService.todos;
    $scope.completedCount = 0;

    $scope.draft = {
      title: "",
      completed: false,
    };

    $scope.addTodo = function () {
      TodosService.addTodo($scope.draft);
      $scope.draft.title = "";
    };

    $scope.clearCompleted = function () {
      TodosService.clearCompleted();

      $scope.todoUpdated();
    };

    $scope.todoUpdated = function () {
      var completed = $scope.todos.list.filter(function (todo) {
        return todo.completed;
      });
      $scope.completedCount = completed.length;
    };

    $scope.todoUpdated();
  });
