console.log(angular.version);

// jQuery(elem).zyx(prop)
// jQuery(elem).zyx(prop,value)

var app = angular.module("myApp", ["todos", "users", "settings", "ngRoute"]);

app.config(function ($routeProvider) {
  $routeProvider
    .when("/users/:id?", {
      templateUrl: "src/users/views/users.tpl.html",
      reloadOnUrl: false,
      controller: "usersCtrl",
      controllerAs: "userVm",
      resolve: {
        title: () => "Users",
        users: (UsersService) => UsersService.fetchUsers(),
      },
    })
    .when("/todos", {
      templateUrl: "src/todos/views/todos.tpl.html",
    })
    .when("/settings", {
      templateUrl: "src/settings/views/settings.tpl.html",
    })
    .when("/something/:id", {
      template: `
      <something 
        title="$resolve.title"
        on-add-user="$resolve.actions.addUser($event)"
      ></something>`,
      resolve: {
        title: ($route) => "Users " + $route.current.params.id,
        actions: (UsersService, $route) => ({
          getUser: () => UsersService.getUserById($route.current.params.id),
          addUser: UsersService.addUser,
        }),
      },
    })
    .otherwise("/users");
});

app.controller("appCtrl", function ($scope, $rootScope, TodosService) {
  $rootScope.title = "Todos";
  $scope.todos = TodosService.todos;
  // $scope.name = 'Anonymous';
  $scope.user = { name: "Anonymous" };

  $scope.views = [
    {
      url: "src/users/views/users.tpl.html",
      label: () => "Users",
    },
    {
      url: "src/todos/views/todos.tpl.html",
      label: () => "Todos " + `${TodosService.todos.list.length}`,
    },
    {
      url: "src/settings/views/settings.tpl.html",
      label: () => "Settings",
    },
  ];

  $scope.currentView = $scope.views[0];

  $scope.changeView = function (view) {
    $scope.currentView = view;
  };
});

app.controller("userCtrl", function ($scope) {
  // $scope.name = '';
  // $scope.$parent.$parent
  // $scope.$root
});

app.constant("todosData", [
  {
    title: "Create Todos Service",
    completed: true,
  },
  {
    title: "Kup mleko",
    completed: true,
  },
  {
    title: "Wynies smieci",
    completed: true,
  },
  {
    title: "Zrób placki",
    completed: true,
  },
  {
    title: "Naucz sie angulara",
    completed: false,
  },
]);

app.value("settings", "placki");

app.config(function (todosData, UsersAPIProvider, $httpProvider) {
  // console.log('app config')
  UsersAPIProvider.setBaseUrl("http://localhost:3000/users");

  // Http Interceptor
  $httpProvider.interceptors.push(function ($q /* ,AuthService */) {
    return {
      request: function (config) {
        // debugger;
        // config.headers.Authorization = "Basic " + AuthService.getToken();
        return config;
      },

      response: function (response) {
        // debugger;
        return response;
      },
    };
  });
});

app.run(function (settings, TodosService, UsersAPI, $http) {
  // console.log('app run')
  $http.defaults.headers.common.Authorization = "Basic YmVlcDpib29w";
});

// angular.bootstrap(document, ["myApp"]); // == ng-app="myApp"
