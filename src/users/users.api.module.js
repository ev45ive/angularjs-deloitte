var usersAPI = angular.module("users.api", []);

usersAPI.constant("API_URL", "http://localhost/users");

function UsersAPI(config, $http) {
  this.config = config;

  this.fetchAll = function () {
    return $http.get(this.config.baseUrl).then(function (resp) {
      return resp.data;
    });
  };

  this.updateUser = function (user) {
    return $http
      .put(this.config.baseUrl + "/" + user.id, user)
      .then(function (resp) {
        return resp.data;
      });
  };

  this.saveUser = function (user) {
    return $http.post(this.config.baseUrl + "/", user).then(function (resp) {
      return resp.data;
    });
  };
}

/* config phase: */
usersAPI.provider("UsersAPI", function (API_URL) {
  var config = {
    baseUrl: API_URL,
  };

  return {
    /**
     * Users Url
     * @param {string} url
     */
    setBaseUrl: function (url) {
      config.baseUrl = url;
    },

    /* Run phase: */
    $get: function ($http) {
      return new UsersAPI(config, $http);
    },
  };
});
