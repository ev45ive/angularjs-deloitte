var users = angular.module("users", ["users.api"]);

// users.factory('SomeLibraryService', function(){
//   return new LibraryService()
// })

users.service("UsersService", function UsersService($http, UsersAPI) {
  var model = (this.model = {
    users: [],
  });

  this.fetchUsers = function () {
    return UsersAPI.fetchAll().then(function (users) {
      model.users = users;
    });
  };

  this.updateUser = function (user) {
    return UsersAPI.updateUser(user) //
      .then((user) => this.fetchUsers().then(() => user));
  };

  this.saveNewUser = function (user) {
    return UsersAPI.saveUser(user).then((user) =>
      this.fetchUsers().then(() => user)
    );
  };
});

users.controller("usersCtrl", function (
  $scope,
  $route,
  UsersService,
  $timeout
) {
  this.model = UsersService.model;
  this.route = $route.current;

  this.selectByRoute = function () {
    var id = $route.current.params.id;
    var user = this.model.users.find((u) => u.id == id);
    if (user) {
      this.mode.selected = user;
    }
  };

  $scope.$on("$routeUpdate", (event, data) => {
    this.selectByRoute();
  });

  modes = {
    default: {},
    selected: { details: true, selected: null, init: () => {} },
    edit: { edit: true, selected: null },
    create: { create: true, selected: null },
    saving: { selected: null, message: "Saving changes.." },
  };

  this.mode = modes.default;
  this.selectByRoute();
  // this.mode = modes.create;

  this.selected = null;

  this.parentSelect = function (user) {
    $route.updateParams({ id: user.id });
    this.mode = modes.selected;
  };

  this.cancel = function () {
    this.mode = modes.default;
  };

  this.editUser = function (user) {
    this.mode = modes.edit;
    this.mode.selected = user;
  };

  this.createNew = function () {
    this.mode = modes.create;
  };

  this.saveNew = function (draft) {
    this.mode = modes.saving;

    UsersService.saveNewUser(draft).then((saved) => {
      $timeout(() => {
        this.mode = modes.selected;
        this.mode.selected = saved;
      }, 2000);
    });
    // .catch((error) => {})
    // .finally(() => {});
  };

  this.save = function (draft) {
    this.mode = modes.saving;

    UsersService.updateUser(draft).then((saved) => {
      $timeout(() => {
        this.mode = modes.selected;
        this.mode.selected = saved;
      }, 2000);
    });
    // .catch((error) => {})
    // .finally(() => {});
  };
});

users.directive("usersList", function () {
  return {
    scope: {
      users: "<",
      selected: "<",
      onUserSelect: "&",
    },
    bindToController: true,
    template: `<div class="list-group">
      <div
        class="list-group-item"
        ng-class="{ active: $ctrl.selected.id === user.id }"
        ng-click="$ctrl.select(user)"
        ng-repeat="user in $ctrl.users"
      >
        {{user.name}}
      </div>
    </div>`,
    controller: function () {
      this.select = function (user) {
        // this.selected = user;
        this.onUserSelect({ $event: user });
      };
    },
    controllerAs: "$ctrl",
  };
});

users.component("userDetails", {
  bindings: {
    user: "<",
    onEdit: "&",
    onCancel: "&",
  },
  template: `<dl>
    <dt>Username:</dt><dd>{{$ctrl.user.username}}</dd>
    <dt>Name:</dt><dd>{{$ctrl.user.name}}</dd>
    <dt>Email:</dt><dd>{{$ctrl.user.email}}</dd>
    <dt>Phone:</dt><dd>{{$ctrl.user.phone}}</dd>
  </dl> 
  <button class="btn btn-info" 
          ng-click="$ctrl.onEdit({$event:$ctrl.user})">
    Edit
  </button>
  <button class="btn btn-info" 
          ng-click="$ctrl.onCancel({$event:$ctrl.user})">
    Cancel
  </button>`,
  // controller: function () {},
  // controllerAs: "$ctrl",
});

users.directive("usernameValid", function ($http, API_URL, $q) {
  return {
    // require: "ngModel",
    // require: ["ngModel"],
    require: { model: "ngModel", form: "?^form", cmp: "?^userEditForm" },
    link: function (scope, $elem, $attr, ctrls, thisCtrl) {
      ctrls.model.$validators.username = function (modelValue, viewValue) {
        var value = modelValue || viewValue;
        return value && !value.includes("batman");
      };

      ctrls.model.$asyncValidators.username = function (modelValue, viewValue) {
        var value = modelValue || viewValue;
        return $http
          .get("http://localhost:3000/users/" + "?username=" + value)
          .then((res) => (res.data.length == 0 ? $q.resolve() : $q.reject()));
      };
    },
  };
});

users.component("userEditForm", {
  bindings: {
    user: "<",
    onSave: "&",
    onCancel: "&",
  },
  template: `<form name="$ctrl.userForm" novalidate ng-submit="$ctrl.save()">
   <!-- <pre>{{ $ctrl.userForm.username | json:2 }}</pre>  -->

    <p class="alert alert-danger" ng-if="$ctrl.message">{{$ctrl.message}}</p>

    <div class="form-group">
      <label>Name:</label>
      <input class="form-control" ng-model="$ctrl.draft.name" name="name" required minlength="3" >
      <div ng-if=" $ctrl.userForm.name.$touched ||  $ctrl.userForm.name.$dirty || $ctrl.userForm.$submitted ">
        <p ng-if=" $ctrl.userForm.name.$error.required">Field is required</p>
        <p ng-if=" $ctrl.userForm.name.$error.minlength">Value is too short</p>
      </div>
    </div>

    <div class="form-group">
      <label>Username:</label>
      <input class="form-control" ng-model="$ctrl.draft.username" name="username" required username-valid minlength="3">
      <div ng-if="  $ctrl.userForm.username.$pending">Please wait... validating</div>
      <div ng-if=" $ctrl.userForm.username.$touched ||  $ctrl.userForm.username.$dirty || $ctrl.userForm.$submitted ">
        <p ng-if=" $ctrl.userForm.username.$error.required">Field is required</p>
        <p ng-if=" $ctrl.userForm.username.$error.username">Username is invalid</p>
        <p ng-if=" $ctrl.userForm.username.$error.minlength">Value is too short</p>
      </div>
    </div>

    <div class="form-group" ng-form="$ctrl.extra">
      <label>Email:</label>
      <input class="form-control" ng-model="$ctrl.draft.email" name="email" required>
      <div ng-if=" $ctrl.userForm.email.$touched ||  $ctrl.userForm.email.$dirty || $ctrl.userForm.$submitted ">
        <p ng-if=" $ctrl.userForm.email.$error.required">Field is required</p>
      </div>
    </div>
    
    <button class="btn btn-success"  type="submit">Save</button>
    <button class="btn btn-danger" ng-click="$ctrl.onCancel()">Cancel</button>
  </form>`,
  controller: function () {
    this.$onInit = function () {
      // console.log("onInit");
    };

    this.$onChanges = function (changes) {
      // changes.user.currentValue
      // console.log("onChanges");
      this.draft = angular.copy(this.user);
      // arguments
      // debugger
    };

    this.$doCheck = function () {
      // console.log("doCheck");
    };

    this.$onDestroy = function () {
      // console.log("onDestroy");
    };

    this.$postLink = function () {
      // console.log("postLink");
      // console.log(this.userForm);
      // console.log(this.extra);
    };

    this.save = function () {
      if (this.userForm.$invalid) {
        this.message = "Cannot save - Check errors";
        return;
      }
      this.onSave({ $event: this.draft });
    };
  },
});
