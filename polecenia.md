# Checkout 
git clone https://bitbucket.org/ev45ive/angularjs-deloitte/

cd angularjs-deloitte/

npm i
npm start

# Proxy
C:\Users\<nazwa uzytk>\.npmrc
C:\Users\<nazwa uzytk>\.gitconfig

git config --global user.name "nazwa"
git config --global user.email "nazwa@nazwa"

# Cwiczenia 
// Nowa galas
git checkout -b cwiczenie_todos origin/master
git pull

// zapisz Cwiczenie
git add . 
git commit -m 

// Pobierz nowe zmiany
git checkout master
git pull

# Project init

npm init
npm init -y

# Depencencies

npm i --save jquery angular bootstrap

# Dev deps - tools

npm i --save-dev http-server

## Instal to %PATH%

npm i -g http-server

http-server

# Package.json SCRIPTS:
package.json => 

```json
"scripts": {
  "start":"http-server"
```
// Start server script:
npm run start

# Webpack + ServiceNow
https://medium.com/@pishchulin/react-in-servicenow-applications-advanced-3e1966fbb817
https://www.npmjs.com/package/servicenow-uploader-webpack-plugin 


# Angular element
Prawym => Zbadaj

angular.element($0).scope()
angular.element($0).controller()
angular.element($0).injector().get('JakisSerwis')

# Component bindings + lifecycle
https://docs.angularjs.org/guide/component#component-based-application-architecture

# NgForm NgModel
https://stackoverflow.com/questions/22414788/angularjs-directive-to-format-date

https://github.com/angular-ui/ui-date
https://angular-ui.github.io/

https://docs.angularjs.org/api/ngMessages
